# Mertsibot

[@Mertsibot](https://t.me/mertsibot) is a 2nd order markov chatbot to be used in telegram chats. Messages are stored in database with column level encryption. (see `db/index.ts` and `crypto/index.ts` for implementation) which makes it more secure to use the chatbot in multiple chats with privacy mode off. 
