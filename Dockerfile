FROM node:10.13.0

WORKDIR /app/chatbot
COPY package.json /app/chatbot
RUN npm install
RUN npm install typescript ts-node -g

COPY . .
RUN tsc -p .
RUN echo "Europe/Helsinki" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

CMD ["node", "dist/index.js"]
