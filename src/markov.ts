/*
    Chatbot, a markov chain chatbot for Telegram
    Copyright (C) 2017  Joonas Ulmanen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    markov.ts
    Simple general implementation of markov chains
    Reinvented the wheel just because felt like it
*/

import { randomFromArray, shuffleArray } from './utils';

export class MarkovTextGenerator {
  private order: number;
  private delimiter: string;
  private mKeys: string[];
  private mPartialKeys: {
    [key: string]: string[];
  };
  private mData: {
    [key: string]: string[];
  };

  constructor(order: number, delimiter: string = '') {
    this.order = order;
    this.delimiter = delimiter;
    this.mKeys = [];
    this.mPartialKeys = {};
    this.mData = {};
  }

  public seed(data: string[]): void {
    data.forEach((row: string) => {
      const splitData = row.split(this.delimiter);
      splitData.forEach((item: string, i: number) => {
        const orderTuple = splitData.slice(i, i + this.order).join(this.delimiter);
        if (!this.mData[orderTuple]) {
          this.mData[orderTuple] = [];
          this.mKeys.push(orderTuple);
          const orderKeys = orderTuple.split(this.delimiter);
          for (const key in orderKeys) {
            if (orderKeys.hasOwnProperty(key)) {
              const partialKey = orderKeys[key];
              if (!this.mPartialKeys[partialKey]) {
                this.mPartialKeys[partialKey] = [];
              }
              this.mPartialKeys[partialKey].push(orderTuple);
            }
          }
        }
        this.mData[orderTuple].push(splitData[i + this.order]);
      });
    });
  }

  public generate(key: string, limit: number): string[] {
    let counter = 0;
    const generated = [key];
    let nextKey = key;
    while (this.mData[nextKey] && counter < limit) {
      counter += 1;
      const nextItem = randomFromArray(this.mData[nextKey]);
      if (!nextItem) {
        break;
      }
      generated.push(nextItem);
      if (this.order > 1) {
        nextKey =
          nextKey
            .split(this.delimiter)
            .slice(1, this.order)
            .join(this.delimiter) +
          this.delimiter +
          nextItem;
      } else {
        nextKey = nextItem;
      }
    }
    return generated;
  }

  public generateFromData(data: string, limit: number): string[] {
    const minResultLength = this.order + 1;
    const splittedData = data.split(this.delimiter);
    const dataKeyMatches = splittedData
      .filter(item => this.mPartialKeys[item])
      .map(item => this.mPartialKeys[item]);
    let result: string[] = [];
    shuffleArray(dataKeyMatches);

    for (
      let dataKeyIndex = 0;
      dataKeyIndex < dataKeyMatches.length && result.length < minResultLength;
      dataKeyIndex++
    ) {
      const candidateKeys = dataKeyMatches[dataKeyIndex];
      shuffleArray(candidateKeys);
      for (
        let candidateKeyIndex = 0;
        candidateKeyIndex < candidateKeys.length && result.length < minResultLength;
        candidateKeyIndex++
      ) {
        const key = candidateKeys[candidateKeyIndex];
        if (!key) {
          continue;
        }
        result = this.generate(key, limit);
      }
    }
    return result.length >= minResultLength ? result : [];
  }
}
