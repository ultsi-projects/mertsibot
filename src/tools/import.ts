/*
    Mertsibot, a markov chain chatbot for Telegram
    Copyright (C) 2018 Joonas Ulmanen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    import.ts
    Import tool for importing text into db
*/

import * as fs from 'fs';
import * as readline from 'readline';
import { decrypt } from '../crypto';
import { saveMsg, selectChatMsgs } from '../db';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

if (process.argv.length <= 3) {
  console.log(
    'Usage: ' +
      __filename +
      ' <chatId> <file> - imports file contents as messages (line separated) for chatId',
  );
  process.exit(-1);
}

const chatId = process.argv[2];
const file = process.argv[3];

if (!chatId || isNaN(Number(chatId))) {
  console.error(`chatId: ${chatId} - no chatId or chatId is not a number, exiting`);
  process.exit(-2);
}

if (!file) {
  console.error('no file, exiting');
  process.exit(-3);
}

fs.readFile(file, async (err, data) => {
  if (err) {
    console.error(err);
    return process.exit(-4);
  }

  const existingMessages = await selectChatMsgs(Number(chatId));
  const first10Messages = existingMessages.rows
    .slice(0, 10)
    .map(val => decrypt(val.msg, chatId))
    .join('\n');
  console.log(`Found ${existingMessages.rowCount} existing messages from db with chatId ${chatId}`);
  console.log(`10 first saved messages in that chat: ${first10Messages}`);

  const lines = data.toString().split('\n');
  console.log(`Found ${lines.length} new messages ready to be imported`);

  rl.question(
    `Do you want to import ${lines.length} messages to chatId ${chatId}?\n`,
    async answer => {
      rl.close();

      if (answer.toLowerCase() !== 'yes') {
        console.log('Aborted.');
        return;
      }

      let count = 0;
      lines.forEach(async line => {
        await saveMsg(Number(chatId), line);
        count++;
        if (count % 25 === 0) {
          console.log(`${count} messages imported`);
        }
        if (count === lines.length) {
          console.log(`${count} messages imported successfully!`);
        }
      });
    },
  );
});
