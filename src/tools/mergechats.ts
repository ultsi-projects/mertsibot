/*
    Mertsibot, a markov chain chatbot for Telegram
    Copyright (C) 2019 Joonas Ulmanen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    mergechats.ts
    Tool to merge messages from 2 chats to 1 chat
    Example: Chat was upgraded to a supergroup by an admin, which
    results in ChatID change, this tool is used to update old messages
    to the new chatid
*/

import * as readline from 'readline';
import { decrypt } from '../crypto';
import { saveMsg, selectChatMsgs, deleteMsgs } from '../db';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

if (process.argv.length <= 3) {
  console.log(
    'Usage: ' +
      __filename +
      ' <oldChatId> <newChatId> - imports messages from oldChatId to newChatId',
  );
  process.exit(-1);
}

const oldChatId = process.argv[2];
const newChatId = process.argv[3];

if (!oldChatId || isNaN(Number(oldChatId))) {
  console.error(`oldChatId: ${oldChatId} - no oldChatId or oldChatId is not a number, exiting`);
  process.exit(-2);
}

if (!newChatId || isNaN(Number(newChatId))) {
  console.error(`newChatId: ${newChatId} - no newChatId or newChatId is not a number, exiting`);
  process.exit(-2);
}

(async function(): Promise<void> {
  try {
    const existingOldMessages = await selectChatMsgs(Number(oldChatId));
    const oldMessagesCount = existingOldMessages.rowCount;
    const first10OldMessages = existingOldMessages.rows
      .slice(0, 10)
      .map(val => decrypt(val.msg, oldChatId))
      .join('\n');
    console.log(`Found ${oldMessagesCount} existing messages from db with oldChatId ${oldChatId}`);
    console.log(`10 first saved messages in that chat: ${first10OldMessages}`);

    const existingNewMessages = await selectChatMsgs(Number(newChatId));
    const first10NewMessages = existingNewMessages.rows
      .slice(0, 10)
      .map(val => decrypt(val.msg, newChatId))
      .join('\n');
    console.log(
      `Found ${existingNewMessages.rowCount} existing messages from db with newChatId ${newChatId}`,
    );
    console.log(`10 first saved messages in that chat: ${first10NewMessages}`);

    rl.question(
      `Are you sure to import ${oldMessagesCount} messages from ${oldChatId} to ${newChatId} with ${existingNewMessages.rowCount} messages? [yes/no]`,
      async (answer: string) => {
        if (answer !== 'yes') {
          console.log('Aborting.');
          return;
        }
        console.log(`Merge started at ${Date.now()}`);

        existingOldMessages.rows.forEach(async (val, index: number) => {
          const decryptedMsg = decrypt(val.msg, oldChatId);
          await saveMsg(Number(newChatId), decryptedMsg);
          if (index % 25 === 0) {
            console.log(`${index} messages imported`);
          }
          if (index >= oldMessagesCount - 1) {
            console.log(`${index} messages imported successfully!`);
            rl.question('Do you want to delete old messages? [yes/no]', async (answer: string) => {
              if (answer !== 'yes') {
                console.log('Done.');
                return;
              }
              await deleteMsgs(Number(oldChatId));
              console.log('Done');
            });
          }
        });
      },
    );
  } catch (err) {
    console.log('Failed to merge chats!!');
    console.log(err);
    process.exit(-5);
  }
})();
