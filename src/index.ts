/*
    Mertsibot, a markov chain chatbot for Telegram
    Copyright (C) 2018 Joonas Ulmanen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    index.ts
    Main entrypoint
*/

import TelegramBot, { Message } from 'node-telegram-bot-api';
import { decrypt } from './crypto';
import { saveMsg, selectChatMsgs, SavedChatMsg } from './db';
import { MarkovTextGenerator } from './markov';
import {
  autoCapitalizeWords,
  isEmoji,
  matchLastCharacter,
  randomFromArray,
  stripMentionsFromWords,
  italicizeHTML,
} from './utils';

const MARKOV_ORDER = 2;
const MARKOV_DELIMITER = ' ';
const MARKOV_RESPONSE_LIMIT = 20;
const BOT_NAME = 'Mertsibot';
const CHANCE_MODIFIERS = { mute: 0.005, talk: -0.02 };
const RANDOM_ENDINGS = [
  '.',
  '?',
  '!',
  '.',
  '?',
  '!',
  '.',
  '?',
  '!',
  '...',
  '!?',
  '???',
  ' :D',
  ' xD',
];
const DEFAULT_CHANCES: {
  [chatId: number]: number;
} = {
  [0]: 0.03,
};

const token = process.env.TOKEN || 'not set';
process.env.TOKEN = '***SECRET***';

if (token === 'not set') {
  throw new Error('TOKEN is not set!');
}

const bot = new TelegramBot(token, { polling: true });

const markovGenerators: {
  [chatId: number]: MarkovTextGenerator;
} = {};

const chanceByChatId: {
  [chatId: number]: number;
} = {};

function decryptChatMsgs(chatId: number, chatIdMsgRows: SavedChatMsg[]): string[] {
  const decryptedMsgs: string[] = chatIdMsgRows.map((row: SavedChatMsg) =>
    decrypt(row.msg, chatId.toString()).toLowerCase(),
  );

  return decryptedMsgs;
}

bot.on('message', async (msg: Message) => {
  if (
    !(msg.text || msg.caption) ||
    msg.chat.type === 'private' ||
    msg.chat.type === 'channel' ||
    !msg.from ||
    msg.from.is_bot ||
    msg.forward_from
  ) {
    return;
  }

  const chatId = msg.chat.id;

  const text = msg.text || msg.caption || '';
  // remove Mentions
  const wordsWithoutMentions = stripMentionsFromWords(text.split(MARKOV_DELIMITER));
  const wholeTextLowerCase = wordsWithoutMentions.join(MARKOV_DELIMITER).toLowerCase();
  const mention = wordsWithoutMentions[0].toLowerCase() === `${BOT_NAME.toLowerCase()}`;
  try {
    // if no markov generator found for chat id yet, create one and try to load old chat messages
    if (!markovGenerators[chatId]) {
      const res = await selectChatMsgs(chatId);
      const generator = new MarkovTextGenerator(MARKOV_ORDER, MARKOV_DELIMITER);
      markovGenerators[chatId] = generator;
      if (res.rows.length > 0) {
        const seedData = decryptChatMsgs(chatId, res.rows);
        seedData.forEach((row: string) =>
          stripMentionsFromWords(row.split(MARKOV_DELIMITER)).join(MARKOV_DELIMITER),
        );
        generator.seed(seedData);
      }
    }

    // react to messages based on chance (prng) or mention
    const markovGenerator = markovGenerators[chatId];
    const defaultChance = DEFAULT_CHANCES[chatId] || DEFAULT_CHANCES[0];
    const chance = chanceByChatId[chatId] || defaultChance;
    const shouldRandomlyTalk = Math.random() > 1 - chance;

    if (shouldRandomlyTalk || mention) {
      const searchText = mention ? `@${wholeTextLowerCase}` : wholeTextLowerCase;
      const generatedResponse = markovGenerator
        .generateFromData(searchText, MARKOV_RESPONSE_LIMIT)
        .map(autoCapitalizeWords)
        .map((word: string) => word.replace(/\\n/g, '\n'));
      if (generatedResponse.length > 0) {
        // make text also end in a random ending
        const lastWord = generatedResponse[generatedResponse.length - 1];
        const randomEnding =
          matchLastCharacter(lastWord, /[\w]/g) && !isEmoji(lastWord)
            ? randomFromArray(RANDOM_ENDINGS)
            : '';
        bot.sendMessage(
          chatId,
          italicizeHTML(
            stripMentionsFromWords(generatedResponse).join(MARKOV_DELIMITER) + randomEnding,
          ),
          { parse_mode: 'HTML' },
        );
        chanceByChatId[chatId] = mention ? chance : chance + CHANCE_MODIFIERS.talk;
      }
    } else {
      chanceByChatId[chatId] =
        chance >= defaultChance ? defaultChance : chance + CHANCE_MODIFIERS.mute;
    }

    // save message if long enough to be useful, and wasn't a chatbot mention
    if (wordsWithoutMentions.length < MARKOV_ORDER + 1 || mention) {
      return;
    }

    await saveMsg(chatId, wordsWithoutMentions.join(MARKOV_DELIMITER));
    markovGenerator.seed([wholeTextLowerCase]);
  } catch (err) {
    console.log(err);
    bot.sendMessage(62461364, `Chatbot error: ${err}`);
  }
});
