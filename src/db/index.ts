/*
    Chatbot, a markov chain chatbot for Telegram
    Copyright (C) 2018 Joonas Ulmanen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    index.ts
    Main entrypoint for db functions
*/

import { Pool, QueryArrayResult } from 'pg';
import { encrypt, hash } from '../crypto';

export type SavedChatMsg = {
  id: number;
  chatId: string;
  msg: string;
  created: string;
};

export type ChatQueryResult = {
  rowCount: number;
  rows: SavedChatMsg[];
};

const DB_URI = process.env.DATABASE_URL || 'not set';

if (DB_URI === 'not set') {
  throw new Error('DATABASE_URL is not set!');
}

process.env.DATABASE_URL = '***OMITTED***';

const dbPool = new Pool({
  connectionString: DB_URI,
});

export async function selectChatMsgs(chatId: number): Promise<ChatQueryResult> {
  return dbPool
    .query('select * from msgs where chatId=$1', [hash(chatId.toString())])
    .then((result: QueryArrayResult) => ({
      rowCount: result.rowCount,
      rows: result.rows.map((val: any) => ({
        id: val.id,
        chatId: val.chatId,
        msg: val.msg,
        created: val.created,
      })),
    }));
}

export async function saveMsg(chatId: number, message: string): Promise<QueryArrayResult> {
  return dbPool.query('insert into msgs (chatId, msg) values ($1, $2)', [
    hash(chatId.toString()),
    encrypt(message, chatId.toString()),
  ]);
}

export async function deleteMsgs(chatId: number): Promise<QueryArrayResult> {
  return dbPool.query('delete from msgs where chatId=$1', [hash(chatId.toString())]);
}
