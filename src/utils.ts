/*
    Mertsibot, a markov chain chatbot for Telegram
    Copyright (C) 2018 Joonas Ulmanen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  utils.ts 
  commonly used functions
*/

export function randomFromArray(array: any[]): any {
  return array[Math.floor(Math.random() * array.length)];
}

export function stripMentionsFromWords(words: string[]): string[] {
  return words.map(word => (word[0] === '@' ? word.substring(1) : word));
}

export function shuffleArray(array: any[]): void {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

const emojiRegex = [/:d+/, /\=d+/, /xd+/, /:p+/, /:\-+d+/, /d+:/, /:c+/];

export function isEmoji(word: string): boolean {
  return word ? emojiRegex.findIndex(emoji => emoji.test(word.toLowerCase())) !== -1 : false;
}

export function matchLastCharacter(str: string, to: RegExp): boolean {
  return str ? to.test(str[str.length - 1]) : false;
}

export function autoCapitalizeWords(word: string, index: number, words: string[]): string {
  const capitalizedWord = word[0].toUpperCase() + word.substring(1);

  return index === 0
    ? capitalizedWord
    : isEmoji(word)
    ? word.toUpperCase()
    : matchLastCharacter(words[index - 1], /[\.\?\!]/)
    ? capitalizedWord
    : word;
}

export function italicizeHTML(text: string): string {
  return `<i>${text}</i>`;
}
